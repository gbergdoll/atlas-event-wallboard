var mongoose = require('mongoose')
    ,Schema = mongoose.Schema;

var KEY = 'CONFIG';

var configSchema = new Schema({
    _id: String,
    eventName: String,
    eventStartYear: Number,
    eventStartMonth: Number,
    eventStartDay: Number,
    eventStartHour: Number,
    eventStartMinute: Number,
    eventEndYear: Number,
    eventEndMonth: Number,
    eventEndDay: Number,
    eventEndHour: Number,
    eventEndMinute: Number,
    message: String,
    twitterWidgetId: String
});

exports.model = mongoose.model('Configuration', configSchema);
exports.key = KEY;