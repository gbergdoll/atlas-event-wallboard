var Config = require('../models/config.js');

exports.index = function(req, res) {
    Config.model.findById(Config.key, function (err, config) {
        if (err || !config) {
            res.render('error', { message: err });
        } else {
            res.render('index',
                {
                    title: config.eventName,
                    eventName: config.eventName,
                    eventStartYear: config.eventStartYear,
                    eventStartMonth: config.eventStartMonth,
                    eventStartDay: config.eventStartDay,
                    eventStartHour: config.eventStartHour,
                    eventStartMinute: config.eventStartMinute,
                    eventEndYear: config.eventEndYear,
                    eventEndMonth: config.eventEndMonth,
                    eventEndDay: config.eventEndDay,
                    eventEndHour: config.eventEndHour,
                    eventEndMinute: config.eventEndMinute,
                    twitterWidgetId: config.twitterWidgetId
                }
            );
        }
    });
};

exports.lastMessage = function(req, res) {
    Config.model.findById(Config.key, function (err, config) {
        if (err || !config) {
            res.send({ message: '' });
        } else {
            res.send({ message: config.message });
        }
    });
};
