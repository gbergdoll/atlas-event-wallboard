var Config = require('../models/config.js');

exports.display = function(req, res){
    Config.model.findById(Config.key, function (err, config) {
        if (err || !config) {
            res.render('error', { message: err });
        } else {
            res.render('configuration', { config: config });
        }
    });
};

exports.define = function(req, res, next){
    var config = {
        _id: Config.key,
        eventName: req.body.eventName,
        eventStartYear: new Date(req.body.eventStartDate).getFullYear(),
        eventStartMonth: new Date(req.body.eventStartDate).getMonth()+1,
        eventStartDay: new Date(req.body.eventStartDate).getDate(),
        eventStartHour: getHours(req.body.eventStartTime),
        eventStartMinute: getMinutes(req.body.eventStartTime),
        eventEndYear: new Date(req.body.eventEndDate).getFullYear(),
        eventEndMonth: new Date(req.body.eventEndDate).getMonth()+1,
        eventEndDay: new Date(req.body.eventEndDate).getDate(),
        eventEndHour: getHours(req.body.eventEndTime),
        eventEndMinute: getMinutes(req.body.eventEndTime),
        message: req.body.message,
        twitterWidgetId: req.body.twitterWidgetId
    };
    Config.model.findByIdAndUpdate(Config.key, config, { upsert: true }, function (err, config) {
        if (err || !config) {
            res.render('error', { message: err });
        } else {
            res.redirect('/');
            /*
            res.render('index',
                {
                    title: config.eventName,
                    eventName: config.eventName,
                    eventStartYear: config.eventStartYear,
                    eventStartMonth: config.eventStartMonth,
                    eventStartDay: config.eventStartDay,
                    eventStartHour: config.eventStartHour,
                    eventStartMinute: config.eventStartMinute,
                    eventEndYear: config.eventEndYear,
                    eventEndMonth: config.eventEndMonth,
                    eventEndDay: config.eventEndDay,
                    eventEndHour: config.eventEndHour,
                    eventEndMinute: config.eventEndMinute,
                    twitterWidgetId: config.twitterWidgetId
                }
            );
            */
        }
    });
};

var getHours = function(timeString) {
    return timeString.split(':')[0];
}

var getMinutes = function(timeString) {
    return timeString.split(':')[1];
}
