$(function () {
    // Update the countdown clock.
    function updateCountdown () {
        var milestone = null,
            caption = null,
            started = ( new Date() >= startDate ),
            finished = ( new Date() >= endDate );

        if (started && finished) {
            milestone = null;
            caption = eventName + ' already ended...';
        } else if (started) {
            milestone = endDate;
            caption = eventName + ' ends in...';
        } else {
            milestone = startDate;
            caption = eventName + ' starts in...';
        }

        var remaining = milestone - new Date();
        displayCountDown(
            finished,
            zeroPadding(Math.max(Math.floor(remaining / 86400000), 0)),
            zeroPadding(Math.max(Math.floor(remaining / 3600000) % 24, 0)),
            zeroPadding(Math.max(Math.floor(remaining / 60000) % 60, 0)),
            zeroPadding(Math.max(Math.floor(remaining / 1000) % 60, 0)),
            caption
        );
  }

    function displayCountDown(finished, days, hours, minutes, seconds, caption) {
        if (finished) {
            $(".digits").hide();
        } else {
            $("#days").html(days);
            $("#hours").html(hours);
            $("#minutes").html(minutes);
            $("#seconds").html(seconds);
        }
        $(".caption").html(caption);
    }

    function zeroPadding(value) {
        return ( value < 10 ? '0'+value : value );
    }

  // Update the dynamic message.
  function updateMessage () {
    $.ajax({
      url: "/last-message"
    }).done(function (response) {
      $("body > .message").html(response.message);
    });
  }

  setInterval(updateCountdown, 1000);
  setInterval(updateMessage, 10000);
  updateCountdown();
  updateMessage();
});
